import React from 'react';
import {BrowserRouter, Route, Switch} from "react-router-dom";
import NavRandomUser from './pages/Navrandomuser.js';
import Home from './pages/Home';
import Swiss from './pages/Swiss';
import Turkey from './pages/Turkey';
import Germany from './pages/Germany';
import fetchAPI from './pages/fetch';
import axiosAPI from './pages/axios';

function App() {
  return (
    <BrowserRouter>
      <NavRandomUser />
        <Switch>
          <Route exact path="/home" component={Home}/>
          <Route exact path="/swiss" component={Swiss}/>
          <Route exact path="/turkey" component={Turkey}/>
          <Route exact path="/germany" component={Germany}/>
          <Route exact path="/fetchAPI" component={fetchAPI}/>
          <Route exact path="/axiosAPI" component={axiosAPI}/>
        </Switch>
    </BrowserRouter>
  );
}

export default App;