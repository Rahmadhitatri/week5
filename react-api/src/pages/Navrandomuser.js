import React from 'react';
import {Link} from 'react-router-dom';

const NavRandomUser = () => {
    return (
        <div>
        <nav className="navbar navbar-expand-lg navbar-dark bg-dark">
          <button className="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNavAltMarkup" aria-controls="navbarNavAltMarkup" aria-expanded="false" aria-label="Toggle navigation">
            <span className="navbar-toggler-icon"></span>
          </button>
          <div className="collapse navbar-collapse" id="navbarNavAltMarkup">
            <div className="navbar-nav">
              <a className="nav-link active" href="/home">Home <span class="sr-only">(current)</span></a>
              <a className="nav-link" href="/swiss">Swiss</a>
              <a className="nav-link" href="/turkey">Turkey</a>
              <a className="nav-link" href="/germany">Germany</a>
              <div className="nav-link">
                  <div className="dropdown">
                    <a className="dropdown-toggle text-light" data-toggle="dropdown" href="#">Rest API</a>
                    <ul className="dropdown-menu" role="menu" aria-labelledby="dLabel">
                      <Link to="/fetchAPI">            
                      <li className="nav-link"><a className="text-secondary" href="#">Fetch API</a></li>
                      </Link>
                      <Link to="/axiosAPI">
                      <li className="nav-link"><a className="text-secondary" href="#">Axios API</a></li>
                      </Link>
                    </ul>
                  </div>
              </div>
            </div>
          </div>
        </nav>
        </div>
    )
};

export default NavRandomUser;