// import logo from './logo.svg';
// import './App.css';
import React from 'react';
import Home from './pages/Home';
import About from './pages/About';
import Contact from './pages/Contact';
import Article from './pages/Article';
import { BrowserRouter, Switch, Route, Link} from "react-router-dom";
import PageNavbar from './pages/pagenav';

function App() {
  return (
    <BrowserRouter>
    <PageNavbar />
      <Switch>
        <Route exact path="/" component={Home}/>
        <Route exact path="/about" component={About}/>
        <Route exact path="/article" component={Article}/>
        <Route exact path="/contact" component={Contact}/>
      </Switch>
    </BrowserRouter>
  );
};

export default App;
